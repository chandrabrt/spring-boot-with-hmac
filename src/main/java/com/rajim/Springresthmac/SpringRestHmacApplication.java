package com.rajim.Springresthmac;

import com.rajim.Springresthmac.server.config.MainConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityFilterAutoConfiguration;
import org.springframework.context.annotation.Import;

/**
 * Here exclude will exclude the spring security basic auth features
 */
@Import({MainConfig.class})
@SpringBootApplication(scanBasePackageClasses = {BasePackage.class},
		exclude = {SecurityAutoConfiguration.class,
				SecurityFilterAutoConfiguration.class })
public class SpringRestHmacApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestHmacApplication.class, args);
	}

}
