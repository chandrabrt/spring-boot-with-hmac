package com.rajim.Springresthmac.server.controller;

import com.rajim.Springresthmac.server.model.TestRequest;
import com.rajim.Springresthmac.server.model.TestResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/test")
public class TestController {

	@PostMapping
	public TestResponse test(@RequestBody TestRequest testRequest) {
		return TestResponse.builder()
				.responseMessage("Your message::"+testRequest.getMessage())
				.build();
	}

	@GetMapping
	public TestResponse test() {
		return TestResponse.builder()
				.responseMessage("Hello Rajim")
				.build();
	}
}
