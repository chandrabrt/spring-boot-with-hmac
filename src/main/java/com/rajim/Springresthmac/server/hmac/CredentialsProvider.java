package com.rajim.Springresthmac.server.hmac;

public interface CredentialsProvider {

    byte[] getApiSecret(String apiKey);
}
