package com.rajim.Springresthmac.server.hmac;

import com.rajim.Springresthmac.commons.HmacSignatureBuilder;
import com.rajim.Springresthmac.server.domain.ClientHMACDetail;
import com.rajim.Springresthmac.server.service.ClientHMACDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Restricts access to resource if HMAC signature is not valid.
 * <p>
 * This filter does not provide Spring {@link org.springframework.security.core.context.SecurityContext} down to filter chain.
 */
public class HmacAccessFilter extends OncePerRequestFilter {

    @Autowired
    private ClientHMACDetailService clientHMACDetailService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {

        final AuthHeader authHeader = HmacUtil.getAuthHeader(request);

        if (authHeader == null) {
            // invalid authorization token
            logger.warn("Authorization header is missing");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        final String apiKey = authHeader.getApiKey();

        final byte[] apiSecret;
        ClientHMACDetail clientHMACDetail =  clientHMACDetailService.loadClientByPublicKey(apiKey);
        if(clientHMACDetail != null) {
            apiSecret = clientHMACDetailService.loadClientByPublicKey(apiKey)
                    .getPrivateKey().getBytes(StandardCharsets.UTF_8);
        } else {
            logger.error("Invalid API key");
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Invalid authorization data");
            return;
        }

        CachingRequestWrapper requestWrapper = new CachingRequestWrapper(request);
        final byte[] contentAsByteArray = requestWrapper.getContentAsByteArray();

        final HmacSignatureBuilder signatureBuilder = new HmacSignatureBuilder()
                .algorithm(authHeader.getAlgorithm())
                .scheme(request.getScheme())
                .host(request.getServerName() + ":" + request.getServerPort())
                .method(request.getMethod())
                .resource(request.getRequestURI())
                .contentType(request.getContentType())
                .date(request.getHeader(HttpHeaders.DATE))
                .nonce(authHeader.getNonce())
                .apiKey(apiKey)
                .apiSecret(apiSecret)
                .payload(contentAsByteArray);

        if (!signatureBuilder.isHashEquals(authHeader.getDigest())) {
            // invalid digest
            logger.error("Invalid digest");
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Invalid authorization data");
            return;
        }

        filterChain.doFilter(requestWrapper, response);
    }
}
