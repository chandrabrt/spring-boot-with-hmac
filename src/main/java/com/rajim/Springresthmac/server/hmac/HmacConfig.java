package com.rajim.Springresthmac.server.hmac;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HmacConfig {

    @Bean
    CredentialsProvider credentialsProvider() {
        return new SimpleCredentialsProvider("user", "secret");
    }
}
