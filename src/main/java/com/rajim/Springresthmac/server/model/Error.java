package com.rajim.Springresthmac.server.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {

    String id;
    String status;
    String code;
    String title;
    String detail;
    List<Link> links;
    Map meta;
    Source source;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Source {
        String pointer;
    }

    public void addPointerSource(String pointer) {
        final Source source = new Source();
        source.pointer = pointer;
        this.source = source;
    }

}
