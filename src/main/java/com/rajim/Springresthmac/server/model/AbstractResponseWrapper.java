package com.rajim.Springresthmac.server.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractResponseWrapper<T> {

    @Valid
    T data;

    @Valid
    List<Error> errors;

    @Valid
    JsonApi jsonapi;

    public AbstractResponseWrapper() {
    }

    public AbstractResponseWrapper(T data) {
        this.data = data;
    }


    public boolean addError(Error error) {
        if (errors == null) {
            errors = new ArrayList<>();
        }
        return errors.add(error);
    }

    public static class JsonApi {
        public static final JsonApi INSTANCE = new JsonApi();

        String version = "1.0";
    }
}
