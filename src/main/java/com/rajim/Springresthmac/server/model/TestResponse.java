package com.rajim.Springresthmac.server.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class TestResponse {

	/**
	 * sample response message
	 */
	private String responseMessage;

}
