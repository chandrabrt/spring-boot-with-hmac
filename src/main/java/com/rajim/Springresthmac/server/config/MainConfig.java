package com.rajim.Springresthmac.server.config;

import com.rajim.Springresthmac.BasePackage;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
		SecurityConfig.class,
})
@ComponentScan(basePackageClasses = {
		BasePackage.class,
})
public class MainConfig {
}
