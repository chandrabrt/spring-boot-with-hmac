package com.rajim.Springresthmac.server.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Setter
@Getter
@Entity
public class ClientHMACDetail {

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true, length = 95)
	private String clientType;

	@Column
	private String publicKey;

	@Column
	private String privateKey;
}
