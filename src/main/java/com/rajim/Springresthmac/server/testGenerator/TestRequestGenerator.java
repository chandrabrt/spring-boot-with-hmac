package com.rajim.Springresthmac.server.testGenerator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rajim.Springresthmac.commons.HmacSignatureBuilder;
import com.rajim.Springresthmac.server.model.Foo;
import com.rajim.Springresthmac.server.model.FooRequestWrapper;
import com.rajim.Springresthmac.server.model.TestRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.nio.charset.StandardCharsets;
import java.time.Clock;

public class TestRequestGenerator {
	public static void main(String[] args) {

		Clock clock = Clock.systemUTC();
		HttpHeaders headers = new HttpHeaders();
		headers.setDate(clock.millis());
		String dateString = ("Thu, 29 Oct 2015 05:27:23 GMT");

		ObjectMapper objectMapper = new ObjectMapper();

		final String valueAsString;
		TestRequest testRequest = new TestRequest();
		testRequest.setMessage("Rajim");

		FooRequestWrapper fooRequestWrapper = new FooRequestWrapper(new Foo("hoho"));
		try {
			valueAsString = objectMapper.writeValueAsString(fooRequestWrapper);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}

		String apiKey = "user";
		String nonce = "4314efa9-04c2-4109-a6a6-385797fa47a3";
		final String resource = "/api/echo";

		final HmacSignatureBuilder signatureBuilder = new HmacSignatureBuilder()
				.algorithm("HmacSHA512")
				.scheme("http")
				.host("localhost:8080")
				.method("POST")
				.resource(resource)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.date(dateString)
				.nonce(nonce)
				.apiKey(apiKey)
				.apiSecret("password")
				.payload(valueAsString.getBytes(StandardCharsets.UTF_8));

		final String signature = signatureBuilder
				.buildAsBase64String();

		final String authHeader = signatureBuilder.getAlgorithm() + " " + apiKey + ":" + nonce + ":" + signature;

		System.out.println(authHeader);
	}
}
