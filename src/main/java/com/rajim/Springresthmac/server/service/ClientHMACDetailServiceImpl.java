package com.rajim.Springresthmac.server.service;

import com.rajim.Springresthmac.server.domain.ClientHMACDetail;
import com.rajim.Springresthmac.server.domain.ClientHMACDetailRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ClientHMACDetailServiceImpl implements ClientHMACDetailService {

	private final ClientHMACDetailRepository clientHMACDetailRepository;

	@Autowired
	public ClientHMACDetailServiceImpl(ClientHMACDetailRepository clientHMACDetailRepository) {
		this.clientHMACDetailRepository = clientHMACDetailRepository;
	}

	@Override
	public ClientHMACDetail loadClientByPublicKey(String username) {
		log.info("Loading client by public key....:: {}", username);
		return clientHMACDetailRepository.loadClientByPublicKey(username);
	}
}
