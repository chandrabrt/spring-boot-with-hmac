package com.rajim.Springresthmac.server.service;

import com.rajim.Springresthmac.server.domain.ClientHMACDetail;

public interface ClientHMACDetailService {
	ClientHMACDetail loadClientByPublicKey(String username);
}
